<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ContactController
Route::get('/', [ContactController::class, 'index']);
Route::get('/contacts/create', [ContactController::class, 'create']);
Route::post('/contacts', [ContactController::class, 'store']);
Route::get('/contacts/{id}', [ContactController::class, 'show']);
Route::get('/contacts/edit/{id}', [ContactController::class, 'edit']);
Route::put('/contacts/update/{id}', [ContactController::class, 'update']);
Route::delete('/contacts/{id}', [ContactController::class, 'destroy']);

//UserController
Route::get('/login', [UserController::class, 'login']);
Route::post('/login/validate', [UserController::class, 'validateLogin']);
Route::get('/register', [UserController::class, 'register']);
Route::post('/register/validate', [UserController::class, 'validateRegister']);
Route::post('/logout', [UserController::class, 'logout']);

