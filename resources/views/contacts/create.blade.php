@extends('layout.main')
@section('title', 'Create Contact')

@section('content')
    <div class="col-md-6 offset-md-3" id="event-create-container">
        <h1>Create Contact</h1>
        <form action="/contacts" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Contact Name">
            </div>
            <div class="form-group">
                <label for="contact">Contact:</label>
                <input type="tel" class="form-control" id="contact" name="contact" placeholder="Contact">
            </div>
            <div class="form-group">
                <label for="city">Email:</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
            </div>
            <input type="submit" value="Create" class="btn btn-primary">
        </form>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
