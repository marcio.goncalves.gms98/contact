@extends('layout.main')
@section('title', 'Create Contact')

@section('content')
    <div class="col-md-6 offset-md-3" id="event-create-container">
        <h1>Update Contact</h1>
        <form action="/contacts/update/{{$contact->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Contact Name" value="{{$contact->name}}">
            </div>
            <div class="form-group">
                <label for="contact">Contact:</label>
                <input type="tel" class="form-control" id="contact" name="contact" placeholder="Contact" value="{{$contact->contact}}">
            </div>
            <div class="form-group">
                <label for="city">Email:</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$contact->email}}">
            </div>
            <input type="submit" value="Update" class="btn btn-primary">
        </form>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
