@extends('layout.main')
@section('title', 'Contact')

@section('content')
    <div class="container contacts-index p-3">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$contact->name}}</h5>
                    <p class="card-text">{{$contact->contact}}</p>
                    <p class="card-text">{{$contact->email}}</p>
                    @auth
                    <div class="d-flex">
                        <div>
                            <a href="/contacts/edit/{{$contact->id}}" class="btn btn-info edit-btn"><ion-icon name="create-outline"></ion-icon> Editar</a>
                        
                        </div>
                        <div>
                            <form action="/contacts/{{$contact->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger delete-btn"><ion-icon name="trash-outline"></ion-icon> Apagar</button>
                            </form>
                        </div>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
        
    </div>
@endsection
