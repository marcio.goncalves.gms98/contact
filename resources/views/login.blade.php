@extends('layout.main')
@section('title', 'Login')

@section('content')
<div class="col-md-6 offset-md-3" id="event-create-container">
    <h1>Login</h1>
    <form action="/login/validate" method="POST">
        @csrf
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <input type="submit" value="Login" class="btn btn-primary">
    </form>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@endsection