@extends('layout.main')
@section('title', 'Contact')

@section('content')
    <div class="container contacts-index p-3">
        <div class="row">
            @foreach ($contacts as $item)
            <div class="card col-md-3">
                <div class="card-body">
                    <h5 class="card-title">{{$item->name}}</h5>
                    <p class="card-text">{{$item->contact}}</p>
                    <div class="d-flex">
                        <div>
                            <a href="/contacts/{{$item->id}}" class="btn btn-warning edit-btn"><ion-icon name="eye"></ion-icon> View</a>
                        </div>
                        @auth
                        <div>
                            <a href="/contacts/edit/{{$item->id}}" class="btn btn-info edit-btn"><ion-icon name="create-outline"></ion-icon> Edit</a>
                        </div>
                        <div>
                            <form action="/contacts/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger delete-btn"><ion-icon name="trash-outline"></ion-icon> Delete</button>
                            </form>
                        </div>
                        @endauth
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
    </div>
@endsection
