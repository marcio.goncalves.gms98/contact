<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveContactRequest;
use App\Models\Contact;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
        return view('contact', ['contacts' => $contacts]);
    }

    public function create()
    {
        $user = auth()->user();
        if ($user)
            return view('contacts.create');
        else
            return redirect('/');
    }

    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('contacts.show', ['contact' => $contact]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:5',
            'contact' => ['required', 'min:8', 'max:255', Rule::unique('contacts')->whereNull('deleted_at')],
            'email' => ['required', 'max:255', Rule::unique('contacts')->whereNull('deleted_at')],
        ]);
        $data = $validated;
        $array['name'] = $data['name'];
        $array['contact'] = $data['contact'];
        $array['email'] = $data['email'];
        $contact = Contact::create($array);
        if ($contact) {
            return redirect('/')->with('msg', 'Contact created successfully!');
        }
    }

    public function edit($id)
    {
        $user = auth()->user();
        if ($user) {
            $contact = Contact::findOrFail($id);
            return view('contacts.edit', ['contact' => $contact]);
        } else {
            return redirect('/');
        }
    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:5',
            'contact' => 'required|max:255',
            'email' => 'required|max:255',
        ]);
        $data = $validated;
        Contact::findOrFail($request->id)->update($data);
        return redirect('/')->with('msg', 'Contact updated successfully!');
    }

    public function destroy($id)
    {
        $user = auth()->user();
        if ($user) {
            Contact::findOrFail($id)->delete();
            $contacts = Contact::all();
            return view('contact', ['contacts' => $contacts]);
        } else {
            return redirect('/');
        }
    }
}
