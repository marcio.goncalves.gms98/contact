<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function validateLogin(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'min:8', 'max:255'],
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/');
        }
        return view('login');
    }

    public function register()
    {
        return view('register');
    }

    public function validateRegister(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'min:5', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->whereNull('deleted_at')],
            'password' => ['required', 'min:8', 'max:255'],
        ]);
        $array['name'] = $validated['name'];
        $array['email'] = $validated['email'];
        $array['password'] = Hash::make($validated['password']);
        $users = User::create($array);
        if ($users) {
            return view('login');
        }
        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
